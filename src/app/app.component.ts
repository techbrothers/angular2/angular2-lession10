import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = "Welcome to Tech Brothers - Tech Solutions";
  user = {
    name : "Jhon",
    age : 30,
    favTechnology: "java"
  };

  alertMe(e) {
    alert("I am parent component");
  }
}
